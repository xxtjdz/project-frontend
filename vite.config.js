import {defineConfig} from 'vite'
import vue from '@vitejs/plugin-vue'
import WindiCSS from 'vite-plugin-windicss'
import path from "path"
// 解构出多个对象要加大括号

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        vue(),
        WindiCSS()
    ],
    resolve: {
        alias: {
            "@": path.resolve(__dirname, "src"),
            "~@": path.resolve(__dirname, "src"),
        },
    },
    // 前端服务器配置
    server: {
        host: 'localhost',
        port: 8081,
        open: false, // 是否打开新窗口
        strictPort: false, // 设为 true 时若端口已被占用则会直接退出，而不是尝试下一个可用端口
        https: false, // 是否开启https
        /*proxy: { // 本地开发环境通过代理实现跨域，生产环境使用 nginx 转发
/!*            // 正则表达式写法
            '^/restful': {
                target: 'http://localhost:9999', // 后端服务实际地址
                changeOrigin: true, //开启代理
                // rewrite: (path) => path.replace(/^\/api/, '')
            },
            '^/sdk': {
                target: 'http://localhost:8081', // 后端服务实际地址
                changeOrigin: true, //开启代理
                // rewrite: (path) => path.replace(/^\/api/, '')
            },*!/
            '/api': {
                target: 'http://localhost:8080/tea',
                changeOrigin: true,
                rewrite: (path) => path.replace(/^\/api/, ''),
            },
        }*/
    },

    // 全局常量
    define: {
        BASE_URL: JSON.stringify(''),
        // BASE_URL: JSON.stringify('/api'),
        // BASE_URL: JSON.stringify('http://127.0.0.1:3000'),
        // BASE_URL: JSON.stringify('http://localhost:8080'),
        // BASE_URL: JSON.stringify('http://127.0.0.1:4523/m1/2524926-0-default/tea'),
        // BASE_URL: JSON.stringify(''),
        TIMEOUT: 10000,
        TITLE: JSON.stringify('茶叶溯源系统'),
    }
})
