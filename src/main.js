import { createApp } from 'vue'
import {createPinia} from "pinia";
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import 'virtual:windi.css'
import App from './App.vue'
import router from "./router/index.js";
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import zhCn from 'element-plus/dist/locale/zh-cn.mjs'
import 'dayjs/locale/zh-cn'
import axios from "axios";

axios.defaults.baseURL = 'http://localhost:8080/tea'
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
const pinia = createPinia()
const app = createApp(App).use(router).use(pinia).use(ElementPlus).use(ElementPlus, {
    locale: zhCn,
});
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}
app.mount('#app')