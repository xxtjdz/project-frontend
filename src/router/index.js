import {createRouter, createWebHashHistory} from "vue-router";
import {useUserStore} from "@/stores/myStore.js";

const routes = [
    {
        path: "/",
        name: "welcome",
        component: () => import("@/views/common/Welcome.vue"),
        children: [
            {
                path: "",
                name: "welcome-login",
                component: () => import("@/components/welcome/LoginPage.vue"),
            },
            {
                path: "register",
                name: "welcome-register",
                component: () => import("@/components/welcome/RegisterPage.vue"),
            },
            {
                path: "forget",
                name: "welcome-forget",
                component: () => import("@/components/welcome/ForgetPage.vue"),
            },
        ],
    },
    {
        path: "/index",
        name: "index",
        component: () => import("~@/views/common/Index.vue"),
    },
    // one part
    {
        path: "/enterpriseinfo",
        name: "Enterpriseinfo",
        component: () => import("~@/pages/onePart/entInfoManage.vue"),
    },
    {
        path: "/upLoadLandInfo",
        name: "UpLoadLandInfo",
        component: () => import("~@/pages/onePart/upLoadLandInfo.vue"),
    },
    {
        path: "/upLoadProductInfo",
        name: "UpLoadProductInfo",
        component: () => import("~@/pages/onePart/upLoadProductInfo.vue"),
    },
    // two part
    {
        path: "/trpcgInfo",
        name: "TrpcgInfo",
        component: () => import("~@/pages/twoPart/trpcgInfo.vue"),
    },
    {
        path: "/trpsyInfo",
        name: "TrpsyInfo",
        component: () => import("~@/pages/twoPart/trpsyInfo.vue"),
    },
    {
        path: "/nszyInfo",
        name: "NszyInfo",
        component: () => import("~@/pages/twoPart/nszyInfo.vue"),
    },
    {
        path: "/iotInfo",
        name: "IotInfo",
        component: () => import("~@/pages/twoPart/iotInfo.vue"),
    },
    {
        path: "/cpscInfo",
        name: "CpscInfo",
        component: () => import("~@/pages/twoPart/cpscInfo.vue"),
    },
    {
        path: "/jcsdInfo",
        name: "JcsdInfo",
        component: () => import("~@/pages/twoPart/jcsdInfo.vue"),
    },
    // three part
    {
        path: "/jggc",
        name: "Jggc",
        component: () => import("~@/pages/threePart/jggc.vue"),
    },
    {
        path: "/storageInfo",
        name: "StorageInfo",
        component: () => import("~@/pages/threePart/storageInfo.vue"),
    },
    {
        path: "/inStorage",
        name: "InStorage",
        component: () => import("~@/pages/threePart/inStorage.vue"),
    },
    {
        path: "/outStorage",
        name: "OutStorage",
        component: () => import("~@/pages/threePart/outStorage.vue"),
    },
    // fourPart
    {
        path: "/khxx",
        name: "Khxx",
        component: () => import("~@/pages/fourPart/khxx.vue"),
    },
    {
        path: "/wlxx",
        name: "Wlxx",
        component: () => import("~@/pages/fourPart/wlxx.vue"),
    },
    {
        path: "/jysj",
        name: "Jysj",
        component: () => import("~@/pages/fourPart/jysj.vue"),
    },
    //fivePart
    {
        path: "/symInfo",
        name: "SymInfo",
        component: () => import("~@/pages/fivePart/symInfo.vue"),
    },
    {
        path: "/symList",
        name: "SymList",
        component: () => import("~@/pages/fivePart/symList.vue"),
    },
    {
        path: "/czshow",
        name: "Czshow",
        component: () => import("~@/pages/sixPart/czshow.vue"),
    },
    {
        path: "/czsystem",
        name: "Czsystem",
        component: () => import("~@/pages/sixPart/czsystem.vue"),
    },
    {
        path: "/proDuctCunzheng",
        name: "ProDuctCunzheng",
        component: () => import("~@/pages/sixPart/proDuctCunzheng.vue"),
    },
    {
        path: "/jiaoyi",
        name: "Jiaoyi",
        component: () => import("~@/pages/sixPart/jiaoyi.vue"),
    },
    {
        path: "/sym",
        name: "Sym",
        component: () => import("~@/pages/fivePart/sym.vue"),
        children: [
            {
                path: ':id',
                name: 'sym-detail',
                component: () => import("~@/pages/fivePart/sym.vue"),
            }
        ]
    },
]

const router = createRouter({
    history: createWebHashHistory(),
    routes
})

router.beforeEach((to, from, next)=> {
    const userStore = useUserStore()
    if (to.fullPath === '/sym') {
        next()
    }
    else if (userStore.auth.user !== null && to.name.startsWith("welcome-")) {
        next('/index')
    } else if (userStore.auth.user === null && to.fullPath.startsWith('/index')) {
        next('/')
    } else if(to.matched.length === 0) {
        next('/index')
    } else {
        next()
    }
})

// 要到导出，别的才能导入
export default router