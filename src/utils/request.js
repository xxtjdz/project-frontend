// axios二次封装
import axios from "axios";
import {ElMessage, ElNotification} from "element-plus";
import querystring from "querystring"
import {get, post} from "@/utils/axiosMethod.js";

const service = axios.create({
    // baseURL: '/api',
    // baseURL: BASE_URL,
    //网络请求的公共配置
    timeout:5000,
});

//在请求被 then 或 catch 处理前拦截它们。发送数据之前
service.interceptors.request.use(req=> {
    if (req.method === "post") {
        req.data = querystring.stringify(req.data)
    }
    //req:包含着网络请求的所有信息
    console.log(req)
    return req
}, error => {
    ElNotification({
        // message: res.data.msg || "请求失败~",
        message: "请求失败~",
        type: 'error',
        duration: 1500
    })
    return  Promise.reject(error)
})

const errorHandle = (status, info)=> {
    switch (status) {
        case 400:
            console.log("语义有误")
            break
        case 401:
            console.log("服务器认证失败")
            break
        case 403:
            console.log("服务器拒绝访问")
            break
        case 404:
            console.log("地址错误")
            break
        case 500:
            console.log("服务器遇到意外")
            break
        case 502:
            console.log("服务器无响应")
            break
        default:
            console.log(info)
            break
    }
}
// 在响应被 then 或 catch 处理前拦截它们，获取数据之前
/*service.interceptors.response.use(res => {
    // console.log(res) //响应的数据
    const {data, status} = res
    console.log(data)
    console.log(status)
    // 有请求响应的时候
    if ( status == 200) {
        // console.log(data.data)
        return data
    } else {
        ElNotification({
            // message: res.data.msg || "请求失败~",
            message: "请求失败~",
            type: 'error',
            duration: 1500
        })
        return Promise.reject(data.msg)
    }
    // const cookies = useCookies()
    // cookies.set(res, res.data.data.token)

}, error => {
    const {res} = error
    //错误的处理才是我们需要最关注的
    error.response && ElMessage.error(error.response.data)
    errorHandle(res.status, res.info)
    return Promise.reject(error)
})*/
service.get = get
service.post = post
export default service