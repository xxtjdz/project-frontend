import {defineStore} from "pinia";
import {computed, reactive, ref} from "vue";

// option store
/*
export const useSymContentStore = defineStore('sym', {
    state: ()=> {
        // data(){return{}} 防止数据污染
        return {SymContent: {}}
    },
    getters: {
        //相对于computed 计算属性
    },
    actions: {
        //相当于methods
    }
})*/

// setup store
export const useSymContentStore = defineStore('sym', ()=> {
    // state
    const symData = ref({})
    const sym = reactive('')
    // getters
    const getterSSymData = computed(()=> {
        return symData.value
    })
    // actions

    return {symData, getterSSymData, sym}
})

export const useUserStore = defineStore('userStore', ()=> {
    const auth = reactive({
        user: null,
/*        qymc: null,
        qyIndex: null,
        active: 0,
        evidence: null,
        qydm: ''*/
    })
    return {auth}
})
export const useCountStore = defineStore('countStore', ()=> {
    const All = reactive({
        evidence: 0,
        upLinkEvi: 0,
        transaction: 0,
        productInfo: 0,
        batchInfo: 0,
        logistics: 0,
        processing: 0
    })
    return {All}
})