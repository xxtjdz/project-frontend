import {get, post} from "@/utils/axiosMethod.js";
import {ElMessage, ElNotification} from "element-plus";

const initFormInfo = (url, form) => {
    try {
        get(url, (data)=> {
            form.value = data
        })
    }  catch (err) {
        console.log("errorOne" + err)
        ElNotification({
            message: "请求失败~",
            type: 'error',
            duration: 1500
        })
    }

}

const initTableInfo =  (url, table, query) => {
    try {
         get(url, (data)=> {
             if (data !== null) {
                 console.log(data)
                 table.value = data.info
             }
             if (query != null) {
                 query.value.totalSize = data.total
             }
        })
    }  catch (err) {
        console.log("errorOne" + err)
        ElNotification({
            message: "请求失败~",
            type: 'error',
            duration: 1500
        })
    }
}
const initWlTableInfo = (url)=> {
/*    try {
        get(url, (data)=> {
            console.log(data)
            tableInfo.value = data.info[index]
            console.log(tableInfo.value)
            clientInfo.value = data.info[index].clientInfo
            console.log(clientInfo.value)
        })
    }  catch (err) {
        console.log("errorOne" + err)
        ElNotification({
            message: "请求失败~",
            type: 'error',
            duration: 1500
        })
    }*/
    return new Promise((resolve, reject) => {
        try {
            get(url, (data)=> {
                console.log(data)
                resolve(data)
            })
        } catch (err) {
            console.log("errorOne" + err);
            ElNotification({
                message: "请求失败~",
                type: "error",
                duration: 1500,
            });
            reject(err);
        }
    });
}
const getOptionInfo = (url) => {
    return new Promise((resolve, reject) => {
        try {
            get(url, (data) => {
                console.log("response data:", data);
                resolve(data);
            });
        } catch (err) {
            console.log("errorOne" + err);
            ElNotification({
                message: "请求失败~",
                type: "error",
                duration: 1500,
            });
            reject(err);
        }
    });
};
const getOptionsInfo = (url, optionName)=> {
    try {
        get(url, (data)=> {
            optionName.value = data
            console.log(data)
        })
    }  catch (err) {
        console.log("errorOne" + err)
        ElNotification({
            message: "请求失败~",
            type: 'error',
            duration: 1500
        })
    }
}
const getAllInfo = (url) =>{
    return new Promise((resolve, reject) => {
        try {
            get(url, (data)=> {
                console.log(data)
                if (typeof data.total !== 'undefined') {
                    resolve(data.total)
                }
                else {
                    console.log(data)
                    resolve(data)
                }
            })
        } catch (err) {
            console.log("errorOne" + err);
            ElNotification({
                message: "请求失败~",
                type: "error",
                duration: 1500,
            });
            reject(err);
        }
    });
}

const confirmLink = (url, getUrl, table, query) => {
    try {
        get(url, (data, msg)=> {
            initTableInfo(getUrl, table, query)
            ElMessage.success(msg)
        })
    }  catch (err) {
        console.log("errorOne" + err)
        ElNotification({
            message: "请求失败~",
            type: 'error',
            duration: 1500
        })
    }
}

export {initFormInfo, initTableInfo, initWlTableInfo, getOptionsInfo, getOptionInfo, getAllInfo, confirmLink}