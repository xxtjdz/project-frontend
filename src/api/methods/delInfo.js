import {myDelete} from "@/utils/axiosMethod.js";
import {ElMessage} from "element-plus";
import {initTableInfo} from "@/api/methods/initInfo.js";

const delTableInfo = (url, table, query, getUrl)=> {
    myDelete(url, (msg)=> {

        initTableInfo(getUrl, table, query)
        ElMessage({
            message: msg,
            type: 'success',
            duration: 1500
        })
    })
}

export {delTableInfo}