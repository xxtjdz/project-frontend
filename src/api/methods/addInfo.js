import {get, post} from "@/utils/axiosMethod.js";
import {ElMessage, ElNotification} from "element-plus";
import {initTableInfo} from "@/api/methods/initInfo.js";

const addTableInfo = (url, data, getUrl, table, query)=> {
    post(url, data.value, (msg)=> {
        initTableInfo(getUrl, table, query)
        ElMessage({
            message: msg,
            type: 'success',
            duration: 1500
        })
    })
}
const addAndGetData = (url, form)=> {
    return new Promise((resolve, reject) => {
        try {
            post(url, form.value, (data)=> {
                console.log(data)
                resolve(data);
            })
        } catch (err) {
            console.log("errorOne" + err);
            ElNotification({
                message: "请求失败~",
                type: "error",
                duration: 1500,
            });
            reject(err);
        }
    });
}
export {addTableInfo, addAndGetData}