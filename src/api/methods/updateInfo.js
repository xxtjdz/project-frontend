import {get, put} from "@/utils/axiosMethod.js";
import {ElMessage, ElNotification} from "element-plus";
import {initFormInfo, initTableInfo} from "@/api/methods/initInfo.js";

const updateFormInfo =  (url, form, getUrl, table, query) => {
    console.log(form.value)
    try {
         put(url, form.value,  (msg)=> {
            initTableInfo(getUrl, table, query)
            ElMessage.success(msg)
        })
    } catch (err) {
        console.log("errorOne" + err)
        ElNotification({
            message: "请求失败~",
            type: 'error',
            duration: 1500
        })
    }
}

const updateEnterpriseForm = (url, form, getUrl)=> {
    console.log(form.value)
    try {
        put(url, form.value,  (msg)=> {
            initFormInfo(getUrl, form)
            ElMessage.success(msg)
        })
    } catch (err) {
        console.log("errorOne" + err)
        ElNotification({
            message: "请求失败~",
            type: 'error',
            duration: 1500
        })
    }
}
const updataInfo = (url, data)=> {
    try {
        put(url, data,  (msg)=> {
            ElMessage.success(msg)
        })
    } catch (err) {
        console.log("errorOne" + err)
        ElNotification({
            message: "请求失败~",
            type: 'error',
            duration: 1500
        })
    }
}
export {updateFormInfo, updateEnterpriseForm, updataInfo}