import {isNull} from "@/api/filters.js";

export function updateUserInfo(act) {
    return "/user/" + act
}

export function entInfoManage() {
    return "/qy/enterpriseinfo"
}

export function updateInfo() {
    return "/qy"
}

export function upLandInfo(queryForm) {
    if (isNull(queryForm.query)) {
        return "/chandi/" + queryForm.pageNum + "/" + queryForm.pageSize
    }
    return "/chandi/" + queryForm.query + "/" + queryForm.pageNum + "/" + queryForm.pageSize
}
export function chandiByStatus() {
    return "/chandi/status"
}
export function addLandInfo() {
    return "/chandi"
}

export function editLandInfo() {
    return "/chandi"
}

export function deleteLandInfo(bh) {
    return "/chandi/" + bh
}

export function upProductInfo(queryForm) {
    if (isNull(queryForm.query)) {
        return "/product/" + queryForm.pageNum + "/" + queryForm.pageSize
    }
    return "/product/" + queryForm.query + "/" + queryForm.pageNum + "/" + queryForm.pageSize
}

export function CntProductInfo() {
    return "/product/cnt"
}

export function addProductInfo() {
    return "/product"
}

export function editProductInfo() {
    return "/product"
}
export function deleteProductInfo(bh) {
    return "/product/" +  bh
}