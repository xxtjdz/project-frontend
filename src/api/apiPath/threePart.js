// 3-1
import {isNull} from "@/api/filters.js";

export function upJgInfo(queryForm) {
    if (isNull(queryForm.query)) {
        return "/process/mgt/" + queryForm.pageNum + "/" + queryForm.pageSize
    }
    return "/process/mgt/" + queryForm.query + "/" + queryForm.pageNum + "/" + queryForm.pageSize
}
export function CntJgInfo() {
    return "/process/mgt/cnt"
}
export function addJgInfo() {
    return "/process/mgt"
}
export function editJgInfo() {
    return "/process/mgt"
}
export function delJgInfo(bh) {
    return "/process/mgt/" + bh
}
// 3-2
export function upStInfo(queryForm) {
    if (isNull(queryForm.query)) {
        return "/storage/mgt/" + queryForm.pageNum + "/" + queryForm.pageSize
    }
    return "/storage/mgt/" + queryForm.query + "/" + queryForm.pageNum + "/" + queryForm.pageSize
}
export function addStInfo() {
    return "/storage/mgt"
}
export function editStInfo() {
    return "/storage/mgt"
}
export function delStInfo(bh) {
    return "/storage/mgt/" + bh
}
// 3-3
export function upIsInfo(queryForm) {
    if (isNull(queryForm.query)) {
        return "/storage/input/" + queryForm.pageNum + "/" + queryForm.pageSize
    }
    return "/storage/input/" + queryForm.query + "/" + queryForm.pageNum + "/" + queryForm.pageSize
}
export function addIsInfo() {
    return "/storage/input"
}
export function editIsInfo() {
    return "/storage/input"
}
export function delIsInfo(id) {
    return "/storage/input/" + id
}
// 3-4
export function upOsInfo(queryForm) {
    if (isNull(queryForm.query)) {
        return "/storage/output/" + queryForm.pageNum + "/" + queryForm.pageSize
    }
    return "/storage/output/" + queryForm.query + "/" + queryForm.pageNum + "/" + queryForm.pageSize
}
export function addOsInfo() {
    return "/storage/output"
}
export function editOsInfo() {
    return "/storage/output"
}
export function delOsInfo(id) {
    return "/storage/output/" + id
}
export function getAllStorage() {
    return "/storage/mgt"
}
