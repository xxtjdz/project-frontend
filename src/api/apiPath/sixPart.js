import {isNull} from "@/api/filters.js";
//6-2
/**
 * 区块链api
 * @returns {string}
 */
export function applyEntCerInfo() {
    return "/entcertificate/apply"
}
export function getEntCerInfo() {
    return "/entcertificate/info"
}
export function modifyEntCerInfo() {
    return "/entcertificate/update"
}
export function confirmEntCerInfo() {
    return "/entcertificate/confirm"
}
// 6-3
export function upProEviInfo(queryForm) {
    if (isNull(queryForm.query)) {
        return "/procertificate/" + queryForm.pageNum + "/" + queryForm.pageSize
    }
    return "/procertificate/" + queryForm.query + "/" + queryForm.pageNum + "/" + queryForm.pageSize
}
export function CntProEviInfo() {
    return "/procertificate"
}
export function delProEviInfo(id) {
    return "/procertificate/" + id
}
export function countUpLink(qymc) {
    return "/procertificate/cnt/" + qymc
}
export function countCerType(type) {
    return "/procertificate/" + type
}
/**
 * 区块链api
 * @returns {string}
 */
export function applyProCerInfo() {
    return "/procertificate/apply"
}
export function getProCerInfo(index) {
    return "/procertificate/info/" + index
}
export function modifyProCerInfo() {
    return "/procertificate/update"
}
export function confirmProCerInfo(index) {
    return "/procertificate/confirm/" + index
}
