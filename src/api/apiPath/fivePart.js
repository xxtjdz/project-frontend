import {isNull} from "@/api/filters.js";

export function upSlInfo(queryForm) {
  if (isNull(queryForm.query)) {
    return "/source/" + queryForm.pageNum + "/" + queryForm.pageSize
  }
  return "/source/" + queryForm.query + "/" + queryForm.pageNum + "/" + queryForm.pageSize
}
export function upSymInfo(sym) {
  return "/source/sym/" + sym
}
export function addSlInfo() {
  return "/source"
}
export function putQrData(pch) {
  return "/source/" + pch
}