// 4-1
import {isNull} from "@/api/filters.js";
export function getKhName() {
    return "/client/info/clientName"
}
export function upKhInfo(queryForm) {
    if (isNull(queryForm.query)) {
        return "/client/info/" + queryForm.pageNum + "/" + queryForm.pageSize
    }
    return "/client/info/" + queryForm.query + "/" + queryForm.pageNum + "/" + queryForm.pageSize
}
export function addKhInfo() {
    return "/client/info"
}
export function editKhInfo() {
    return "/client/info"
}
export function delKhInfo(id) {
    return "/client/info/" + id
}
export function getAllClientName() {
    return "/client/info"
}
// 4-2
export function upWlInfo(queryForm) {
    if (isNull(queryForm.query)) {
        return "/transport/info/" + queryForm.pageNum + "/" + queryForm.pageSize
    }
    return "/transport/info/" + queryForm.query + "/" + queryForm.pageNum + "/" + queryForm.pageSize
}
export function CntWlInfo() {
    return "/transport/info"
}
export function addWlInfo() {
    return "/transport/info"
}
export function editWlInfo() {
    return "/transport/info"
}
export function delWlInfo(id) {
    return "/transport/info/" + id
}
export function getWlInfo(id) {
    return "/transport/info/" + id
}
// 4-3
export function upJyInfo(queryForm) {

    if (isNull(queryForm.query)) {
        return "/transaction/info/" + queryForm.pageNum + "/" + queryForm.pageSize
    }
    return "/transaction/info/" + queryForm.query + "/" + queryForm.pageNum + "/" + queryForm.pageSize
}
export function CntJyInfo() {
    return "/transaction/info/cnt"
}
export function addJyInfo() {
    return "/transaction/info"
}
export function editJyInfo() {
    return "/transaction/info"
}
export function delJyInfo(id) {
    return "/transaction/info/" + id
}