import {isNull} from "@/api/filters.js";

// 2-1-1
export function upCgInfo(queryForm) {
    if (isNull(queryForm.query)) {
        return"/input/purchase/" + queryForm.pageNum + "/" + queryForm.pageSize
    }
    return "/input/purchase/" + queryForm.query + "/" + queryForm.pageNum + "/" + queryForm.pageSize
}
export function addCgInfo() {
    return "/input/purchase"
}
export function editCgInfo() {
    return "/input/purchase"
}
export function delCgInfo(bh) {
    return "/input/purchase/" + bh
}
export function djzhInfo(djzh) {
    return "/input/" + djzh
}
//2-1-2
export function upUseInfo(queryForm) {
    if (isNull(queryForm.query)) {
        return"/input/use/" + queryForm.pageNum + "/" + queryForm.pageSize
    }
    return "/input/use/" + queryForm.query + "/" + queryForm.pageNum + "/" + queryForm.pageSize
}
export function addUseInfo() {
    return "/input/use"
}
export function editUseInfo() {
    return "/input/use"
}
export function delUseInfo(bh) {
    return "/input/use/" + bh
}
export function djzhUseInfo(djzh) {
    return "/input/" + djzh
}
//2-2-1
export function upNsInfo(queryForm) {
    if (isNull(queryForm.query)) {
        return"/farm/work/" + queryForm.pageNum + "/" + queryForm.pageSize
    }
    return "/farm/work/" + queryForm.query + "/" + queryForm.pageNum + "/" + queryForm.pageSize
}
export function addNsInfo() {
    return "/farm/work"
}
export function editNsInfo() {
    return "/farm/work"
}
export function delNsInfo(bh) {
    return "/farm/work/" + bh
}
// 获取所有的产地名称
export function getAllCdmc() {
    return "/chandi"
}
export function getNsbyZyr(queryForm) {
    return "/farm/work/all/" + queryForm.value.query + "/" + queryForm.value.pageNum + "/" + queryForm.value.pageSize
}
// 2-2-2
export function getAllIotCdmc() {
    return "/production/environment"
}
export function upIotInfo(queryForm) {
    if (isNull(queryForm.query)) {
        return"/production/environment/" + queryForm.pageNum + "/" + queryForm.pageSize
    }
    return "/production/environment/" + queryForm.query + "/" + queryForm.pageNum + "/" + queryForm.pageSize
}
export function delIotInfo(bh) {
    return "/production/environment/" + bh
}
export function addIotInfo() {
    return "/production/environment"
}
export function editIotInfo() {
    return "/production/environment"
}
//2-3
export function upcpscInfo(queryForm) {
    if (isNull(queryForm.query)) {
        return "/goods/batch/" + queryForm.pageNum + "/" + queryForm.pageSize
    }
    return "/goods/batch/" + queryForm.query + "/" + queryForm.pageNum + "/" + queryForm.pageSize
}
export function getPchInfo(cpmc) {
    return "/goods/batch/" + cpmc
}
export function CntCpInfo() {
    return "/goods/batch/cnt"
}
export function delCpInfo(bh) {
    return "/goods/batch/" + bh
}
export function addCpInfo() {
    return "/goods/batch"
}
export function editCpInfo() {
    return "/goods/batch"
}
export function getAllPcCpmc() {
    return "/goods/batch"
}
export function getAllCpmc() {
    return "/product"
}
// 2-4
export function upJcInfo(queryForm) {
    if (isNull(queryForm.query)) {
        return"/detection/info/" + queryForm.pageNum + "/" + queryForm.pageSize
    }
    return "/detection/info/" + queryForm.query + "/" + queryForm.pageNum + "/" + queryForm.pageSize
}
export function addJcInfo() {
    return "/detection/info"
}
export function editJcInfo() {
    return "/detection/info"
}
export function delJcInfo(bh) {
    return "/detection/info/" + bh
}
export function getPchByCpmc(cpmc) {
    return "/detection/info/" + cpmc
}
export function getCpmc() {
    return "/detection/info/cpmc"
}
