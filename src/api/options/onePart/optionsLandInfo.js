export const optionsLandInfo = [
    {
        label: "编号",
        prop: 'bh',
        width: "60"
    },
    {
        label: "生产企业名称",
        prop: 'qymc',
        width: "200"
    },
    {
        label: "地块名称",
        prop: 'chandi',
        width: "200"
    },
    {
        label: "地块详细地址",
        prop: 'detailaddress',
        width: "230"
    },
    {
        label: "地块负责人",
        prop: 'cdfzr',
        width: "160"
    },
    {
        label: "地块实图",
        prop: 'cdt',
        width: "160"
    },
    {
        label: "用途",
        prop: 'yt',
        width: "200"
    },
    {
        label: "操作",
        prop: 'action',
        width: "150"
    }
]