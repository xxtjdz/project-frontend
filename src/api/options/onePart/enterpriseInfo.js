export const enterpriseInfo = [
    {
        label: "企业名称",
        prop: 'qymc',
        onlyRead: true
    },
    {
        label: "企业负责人",
        prop: 'fzr',
        onlyRead: true
    },
    {
        label: "负责人手机号",
        prop: 'fzrtel',
        onlyRead: false
    },
    {
        label: "企业信用代码",
        prop: 'xydm',
        onlyRead: true
    },
    {
        label: "企业地址",
        prop: 'addr',
    },
    {
        label: "主营业务",
        prop: 'zyyw',
    },
    {
        label: "主营产品",
        prop: 'zycp',
    },
    {
        label: "经营规模",
        prop: 'jygm',
    },
    {
        label: "企业简介",
        prop: 'dwjj',
    }
]