export const optionsProductInfo = [
    {
        label: "编号",
        prop: 'bh',
        width: "60"
    },
    {
        label: "生产企业名称",
        prop: 'qymc',
        width: "200"
    },
    {
        label: "产品名称",
        prop: 'cpmc',
        width: "120"
    },
    {
        label: "产品图片",
        prop: 'cptp',
        width: "210"
    },
    {
        label: "产品规格",
        prop: 'cpgg',
        width: "120"
    },
    {
        label: "保质期（单位: 月）",
        prop: 'cpbzq',
        width: "150"
    },
    {
        label: "生产日期",
        prop: 'scrq',
        width: "120"
    },
    {
        label: "产品介绍",
        prop: 'cpjs',
    },
    {
        label: "操作",
        prop: 'action',
    }
]