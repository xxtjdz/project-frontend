export const iotInfo = [
    {
        label: "编号",
        prop: 'bh',
        width: "60"
    },
    {
        label: "生产企业名称",
        prop: 'qymc',
    },
    {
        label: "产地名称",
        prop: 'chandi',
    },
    {
        label: "PH值",
        prop: 'ph',
    },
    {
        label: "土壤温度",
        prop: 'trwd',
    },
    {
        label: "土壤湿度",
        prop: 'trsd',
    },
    {
        label: "更新日期",
        prop: 'gxrq',
    },
    {
        label: "操作",
        prop: 'cpaction',
    }
]