export const nsInfo = [
    {
        label: "编号",
        prop: 'bh',
        width: "60"
    },
    {
        label: "生产企业名称",
        prop: 'qymc',
        width: "130"
    },
    {
        label: "产地名称",
        prop: 'cdmc',
    },
    {
        label: "农事作业",
        prop: 'nszy',
    },
    {
        label: "投入品名称",
        prop: 'trpmc',
    },
    {
        label: "作业人",
        prop: 'zyr',
    },
    {
        label: "作业量",
        prop: 'zyl',
    },
    {
        label: "日期",
        prop: 'rq',
    },
    {
        label: "操作",
        prop: 'action',
        width: "160"
    }
]