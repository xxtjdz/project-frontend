export const cpInfo = [
    {
        label: "编号",
        prop: 'bh',
        width: "80"
    },
    {
        label: "生产企业名称",
        prop: 'qymc',
    },
    {
        label: "产品名称",
        prop: 'cpmc',
    },
    {
        label: "产品规格",
        prop: 'cpgg',
    },
    {
        label: "生产负责人",
        prop: 'scfzr',
    },
    {
        label: "产地",
        prop: 'chandi',
    },
    {
        label: "生产日期",
        prop: 'scrq',
    },
    {
        label: "批次号",
        prop: 'pch',
    },
    {
        label: "操作",
        prop: 'cpaction',
    }
]