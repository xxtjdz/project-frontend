export const useInfo = [
    {
        label: "编号",
        prop: 'bh',
        width: "60"
    },
    {
        label: "生产企业名称",
        prop: 'qymc',
        width: "130"
    },
    {
        label: "投入品名称",
        prop: 'trpmc',
        width: "100"
    },
    {
        label: "投入品登记证号",
        prop: 'nydjzh',
        width: "150"
    },
    {
        label: "生产商",
        prop: 'nyscz',
        width: "160"
    },
    {
        label: "经手人",
        prop: 'jsr',
        width: "80"
    },
    {
        label: "使用去向",
        prop: 'syqx',
        width: "100"
    },
    {
        label: "使用对象",
        prop: 'sydx',
        width: "100"
    },
    {
        label: "规格",
        prop: 'trpgg',
        width: "100"
    },
    {
        label: "使用数量",
        prop: 'sysl',
        width: "80"
    },
    {
        label: "使用方法",
        prop: 'nysyfa',
        width: "110"
    },
    {
        label: "使用日期",
        prop: 'syrq',
        width: "110"
    },
    {
        label: "操作",
        prop: 'action',
        width: "160"
    }
]