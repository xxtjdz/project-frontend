export const purchaseInfo = [
    {
        label: "编号",
        prop: 'bh',
        width: "60"
    },
    {
        label: "生产企业名称",
        prop: 'qymc',
        width: "130"
    },
    {
        label: "投入品名称",
        prop: 'trpmc',
        width: "100"
    },
    {
        label: "投入品登记证号",
        prop: 'nydjzh',
        width: "130"
    },
    {
        label: "生产商",
        prop: 'nyscz',
        width: "100"
    },
    {
        label: "作物范围",
        prop: 'nyzw',
        width: "100"
    },
    {
        label: "防治对象",
        prop: 'nyfz',
    },
    {
        label: "使用方法",
        prop: 'nysyfa',
    },
    {
        label: "规格",
        prop: 'trpgg',
        width: "90"
    },
    {
        label: "采购数量",
        prop: 'cgsl',
        width: "80"
    },
    {
        label: "经手人",
        prop: 'jsr',
        width: "80"
    },
    {
        label: "采购来源",
        prop: 'ly',
        width: "90"
    },
    {
        label: "采购日期",
        prop: 'rq',
        width: "120"
    },
    {
        label: "操作",
        prop: 'action',
        width: "150"
    }
]