export const addPurchaseInfo = [
    {
        label: "投入品登记证号",
        prop: 'nydjzh',
        width: "150"
    },
    {
        label: "投入品名称",
        prop: 'trpmc',
        width: "100"
    },
    {
        label: "生产商",
        prop: 'nyscz',
        width: "150"
    },
    {
        label: "作物范围",
        prop: 'nyzw',
        width: "100"
    },
    {
        label: "防治对象",
        prop: 'nyfz',
    },
    {
        label: "使用方法",
        prop: 'nysyfa',
    },
    {
        label: "采购来源",
        prop: 'ly',
    },
    {
        label: "规格",
        prop: 'trpgg',
        width: "70"
    },

    {
        label: "采购数量",
        prop: 'cgsl',
        width: "70"
    },
    {
        label: "经手人",
        prop: 'jsr',
    },
    {
        label: "备注",
        prop: 'bz',
    },
    {
        label: "采购日期",
        prop: 'rq',
    },
]

/*const form = ref({
  'nydjzh': '',
  'trpmc': '',
  'nyscz': '',
  'nyzw': '',
  'nyfz': '',
  'nysyfa': '',
  'ly': '',
  'trpgg': '',
  'cgsl': '',
  'jsr': '',
  'bz': '',
  'rq': '',
})*/