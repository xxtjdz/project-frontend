export const JcInfo = [
    {
        label: "编号",
        prop: 'bh',
        width: "60"
    },
    {
        label: "企业名称",
        prop: 'qymc',
        width: "180"
    },
    {
        label: "批次号",
        prop: 'pch',

    },
    {
        label: "样本名称",
        prop: 'cpmc',
        width: "100"
    },
    {
        label: "检测内容",
        prop: 'jcnr'
    },
    {
        label: "检测值",
        prop: 'jcz',
        width: "80"
    },
    {
        label: "检测结果",
        prop: 'jcjg',
        width: "80"
    },
    {
        label: "检测地点",
        prop: 'jcdd',
    },
    {
        label: "检测人",
        prop: 'jcr',
        width: "80"
    },
    {
        label: "检测日期",
        prop: 'rq',
        width: "120"
    },
    {
        label: "检测报告",
        prop: 'pic',
    },
    {
        label: "操作",
        prop: 'action',
        width: "140"
    }
]