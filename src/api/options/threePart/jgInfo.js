export const JgInfo = [
    {
        label: "编号",
        prop: 'id',
        width: "60"
    },
    {
        label: "生产企业名称",
        prop: 'qymc',

    },
    {
        label: "产品名称",
        prop: 'cpmc'
    },
    {
        label: "加工批次号",
        prop: 'pch',

    },
    {
        label: "加工图片",
        prop: 'processPic'
    },
    {
        label: "加工过程",
        prop: 'processName',

    },
    {
        label: "加工方式",
        prop: 'processWay',
    },
    {
        label: "加工时间",
        prop: 'processTime',
    },
    {
        label: "操作人",
        prop: 'fzr',
        width: "100"
    },
    {
        label: "操作",
        prop: 'action',
    }
]