export const IsInfo = [
    {
        label: "编号",
        prop: 'id',
        width: "60"
    },
    {
        label: "供应商",
        prop: 'supplyName',

    },
    {
        label: "入库货物",
        prop: 'cpmc'
    },
    {
        label: "入库仓库",
        prop: 'storageName',

    },
    {
        label: "入库数量",
        prop: 'inNumber'
    },
    {
        label: "入库时间",
        prop: 'addDate',
    },
    {
        label: "备注",
        prop: 'comment',
    },
    {
        label: "操作",
        prop: 'action',
    }
]