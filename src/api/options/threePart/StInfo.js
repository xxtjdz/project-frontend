export const StInfo = [
    {
        label: "编号",
        prop: 'storageId',
        width: "100"
    },
    {
        label: "仓库名称",
        prop: 'storageName',

    },
    {
        label: "仓库容量",
        prop: 'storageRl',
        width: "180"
    },
    {
        label: "仓库负责人电话",
        prop: 'storageDh',
        width: "180"
    },
    {
        label: "仓库地址",
        prop: 'storageAddr'
    },
    {
        label: "创建时间",
        prop: 'cjsj',
    },
    {
        label: "操作",
        prop: 'action',
    }
]