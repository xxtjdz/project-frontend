export const OsInfo = [
    {
        label: "编号",
        prop: 'id',
        width: "60"
    },
    {
        label: "供应商",
        prop: 'supplyName',

    },
    {
        label: "出库货物",
        prop: 'cpmc'
    },
    {
        label: "出库仓库",
        prop: 'storageName',

    },
    {
        label: "出库数量",
        prop: 'outNumber'
    },
    {
        label: "出库时间",
        prop: 'addDate',
    },
    {
        label: "备注",
        prop: 'comment',
    },
    {
        label: "操作",
        prop: 'action',
    }
]