export const productFileInfo = [
  {
    label: "产品介绍",
    prop: "cpjs",
  },
  {
    label: "产品名称",
    prop: "cpmc",
  },
  {
    label: "生产企业",
    prop: "qymc",
  },
  {
    label: "生产日期",
    prop: "scrq",
  },
  {
    label: "包装规格",
    prop: "cpgg",
  },
  {
    label: "保质期",
    prop: "cpbzq",
  },
  {
    label: "企业电话",
    prop: "fzrtel",
  },
  {
    label: "产品检验",
    prop: "jcjg",
  },
  {
    label: "检验报告",
    prop: "pic",
  },
  {
    label: "溯源码",
    prop: "sym",
  },
  {
    label: "扫码次数",
    prop: "smcs",
  },
];
// 产品名称->产地名称->onePartContent
