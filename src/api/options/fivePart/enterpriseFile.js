export const enterpriseFile = [
  {
    label: "有机认证",
    prop: "yjpic",
  },
  {
    label: "绿色食品",
    prop: "lspic",
  },
  {
    label: "无公害农产品",
    prop: "wghpic",
  },
  {
    label: "农产品地理标志",
    prop: "dlpic",
  },
  {
    label: "生态原产地保护产品（PEOP）",
    prop: "qtpic",
  },
];

