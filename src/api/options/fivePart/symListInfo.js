export const symListInfo = [
  {
    label: "产品名称",
    prop: "cpmc",
  },
  {
    label: "产地",
    prop: "chandi",
  },
  {
    label: "批次号",
    prop: "pch",
  },
  {
    label: "溯源码",
    prop: "sym",
  },
  {
    label: "扫码次数",
    prop: "smcs",
  },
  {
    label: "溯潮源码生成时间",
    prop: "date",
  },
];
