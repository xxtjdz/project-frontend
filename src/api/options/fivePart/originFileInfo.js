export const originFileInfo = [
  {
    label: "产地名称",
    prop: "chandi",
  },
  {
    label: "产地地址",
    prop: "detailAddress",
  },
  {
    label: "产地负责人",
    prop: "cdfzr",
  },
  {
    label: "土壤温度",
    prop: "trwd",
  },
  {
    label: "土壤湿度",
    prop: "trsd",
  },
  {
    label: "PH值",
    prop: "ph",
  },
  {
    label: "产地图",
    prop: "cdt",
  },
];
// 产品名称->产地名称->onePartContent
