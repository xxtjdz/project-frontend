export const KhInfo = [
    {
        label: "编号",
        prop: 'id',
        width: "100"
    },
    {
        label: "客户企业名称",
        prop: 'clientFirm',

    },
    {
        label: "客户姓名",
        prop: 'clientName',
        width: "180"
    },
    {
        label: "客户电话",
        prop: 'clientPhone',
        width: "200"
    },
    {
        label: "客户来源",
        prop: 'clientSource',
        width: "190"
    },
    {
        label: "客户地址",
        prop: 'clientAddress',
    },
    {
        label: "操作",
        prop: 'action',
    }
]