export const WlInfo = [
    {
        label: "编号",
        prop: 'id',
        width: "55"
    },
    {
        label: "发货地址",
        prop: 'shipAddress',

    },
    {
        label: "目的地",
        prop: 'destination'
    },
    {
        label: "发货人",
        prop: 'shiperName',
        width: "70"
    },
    {
        label: "运输联系方式",
        prop: 'carrierPhone'
    },
    {
        label: "发货时间",
        prop: 'deliveryTime',
        width: "100"
    },
    {
        label: "操作",
        prop: 'action',
        width: "100"
    }
]