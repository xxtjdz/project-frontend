export const JyInfo = [
    {
        label: "编号",
        prop: 'id',
        width: "60"
    },
    {
        label: "企业名称",
        prop: 'qymc',

    },
    {
        label: "客户姓名",
        prop: 'name'
    },
    {
        label: "交易金额",
        prop: 'money',

    },
    {
        label: "交易产品",
        prop: 'product'
    },
    {
        label: "交易数量",
        prop: 'number',
    },
    {
        label: "运输数量",
        prop: 'number',
    },
    {
        label: "交易图片",
        prop: 'picture',
    },
    {
        label: "交易时间",
        prop: 'time',
    },
    {
        label: "操作",
        prop: 'action',
    }
]