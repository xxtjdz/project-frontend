export const proEviInfo = [
    {
        label: "编号",
        prop: 'id',
        width: "60"
    },
    {
        label: "生产企业名称",
        prop: 'qymc',
        width: 180
    },
    {
        label: "产品名称",
        prop: 'cpmc',
        width: 100
    },
    {
        label: "证书图片",
        prop: 'certificateMaterial',
        width: 130
    },
    {
        label: "证书类型",
        prop: 'certificateType',
        width: 120
    },
    {
        label: "证书到期时间",
        prop: 'expireTime',
        width: 110
    },
    {
        label: "是否上链",
        prop: 'status',
        width: 90
    },
    {
        label: "上链凭证",
        prop: 'certificate',
    },
    {
        label: "操作",
        prop: 'upLink',
        width: 200
    }
]