export const qyLinkInfo = [
    {
        label: "企业名称",
        prop: 'qymc',
    },
    {
        label: "上链凭证",
        prop: 'evidence',
    },
    {
        label: "企业负责人",
        prop: 'fzr',
    },
    {
        label: "企业信用代码",
        prop: 'xydm',
    },
    {
        label: "营业执照",
        prop: 'businessLicense',
    },
    {
        label: "身份证正面",
        prop: 'cardX',
    },
    {
        label: "身份证反面",
        prop: 'cardY',
    },
    {
        label: "其他图片",
        prop: 'qtpic',
    },
]