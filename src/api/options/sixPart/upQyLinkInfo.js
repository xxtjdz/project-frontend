export const upQyLinkInfo = [
    {
        label: "营业执照",
        prop: 'businessLicense',
    },
    {
        label: "身份证正面",
        prop: 'cardX',
    },
    {
        label: "身份证反面",
        prop: 'cardY',
    },
    {
        label: "其他图片",
        prop: 'qtpic',
    },
]