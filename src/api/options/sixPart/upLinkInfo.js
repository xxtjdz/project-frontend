export const upLinkInfo = [
    {
        label: "上链地址",
        prop: 'address',
    },
/*    {
        label: "生产企业名称",
        prop: 'qymc',
    },
    {
        label: "产品名称",
        prop: 'cpmc',
    },*/
    {
        label: "上链证书",
        prop: 'certificateMaterial',
    },
    {
        label: "证书类型",
        prop: 'certificateType',
    },
/*    {
        label: "证书到期时间",
        prop: 'expireTime',
    },*/
    {
        label: "上链时间",
        prop: 'certificateTime',
    },
/*    {
        label: "上链凭证",
        prop: 'certificate',
    },*/
]