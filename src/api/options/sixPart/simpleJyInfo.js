export const simpleJyInfo = [
    {
        label: "交易时间",
        prop: 'time',
    },
    {
        label: "交易产品",
        prop: 'product'
    },
    {
        label: "交易金额",
        prop: 'money',

    },
    {
        label: "交易数量",
        prop: 'number',
    },
]