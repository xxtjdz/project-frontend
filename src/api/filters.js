export const isNull = (data) => {
    if (!data) return true
    else if (JSON.stringify(data) === '{}') return true
    else if (JSON.stringify(data) === '[]') return true
}

/*
const dayjs = require('dayjs')
const filterTimes = (val, format = 'YYYY-MM-DD') => {
    if (!isNull(val)) {
        val = parseInt(val)
        return dayjs(val).format(format)
    } else {
        return '--'
    }
}*/
