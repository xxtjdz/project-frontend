import service from "@/utils/request.js";
import {ElMessage} from "element-plus";
import {useRouter} from "vue-router";
const router = useRouter()
// 向后端发请求
export function login(username, password, groupName) {
    return service.post("/user/login", {
        username,
        password,
        groupName
    }, (message)=> {
        ElMessage({
            message,
            type: 'success',
            duration: 1000
        })
        router.push('/index')
    })
        // .then(function (res) {
        //     console.log(res)
        //
        // })
        // .catch(function (error) {
        //     console.log(error);
        // });
}
